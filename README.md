# Enmascarar con SIMD

# Contenido

* [Introducción](#introducción)
* [Requisitos](#requisitos)
* [Implementación en C](#implementación-en-c)
* [Implementación en NASM](#implementación-en-nasm)
* [Demo](#demo)
* [Autores](#autores)

# Introducción
TP 2 de Organización del computador 2 2021 2do semestre

Se realizo el enmascaramiento de dos imagenes, donde se tomo a partir de una mascara (en blanco y negro) y se determino que las posiciones de los pixeles de la mascara que son negros, pertenezcan a la imagen 1 y que los blancos pertenezcan a la imagen 2.

Para poder ejecutar todo el codigo, se creo el Programa.c que llama a los enmascarar en c y asm, que estan colocados en subcarpetas, los llamamos con los comandos cargados en el archivo bash.sh

# Requisitos
- SO Ubuntu 16.04
- GCC v5.4.0
- NASM v2.11.08
- GraphicsMagick

# Implementación en C
## Procedimiento:
1. Creamos todos los buffer que necesitamos.
2. Pedimos memoria con malloc para todos los buffer.
3. Leemos los archivos rgb, img1.rgb, img2.rgb, mascara.rgb, los cuales asignamos a las buffer A, B y Mascara.
4. Separamos cada unos de los buffers anteriores, en 3 nuevos buffers para tener un buffer red, green y blue, para cada buffer de cada imagen.
5. Aplicamos la mascara a cada uno de estos buffers, usamos el aplica_mascara, que se encarga de asignar al bufferResultado (sea red, green o blue) el resultado.
6. Combinamos los 3 buffers del resultado en un unico bufferResultado.
7. Escribimos el resultado final en el archivo salida_c.
8. Por ultimo liberamos la memoria que habiamos solicitado anteriormente para el buffer.

# Implementación en NASM
## Procedimiento:

1. Se creó los buffer con C para poder acceder a las imágenes en el archivo Enmascara-params.c.
2. Se creó la fución *emascarar_asm()* en NASM  el cual recibe como parámetro los buffers de la imagen 1, imagen 2 y la imagen máscara y también el tamaño total de las 3 imágenes (es obligatorio que las 3 imágenes tengan el mismo tamaño).
3. Se llama la función enmascarar_asm() desde C y se le pasan los buffers mencionados y el tamaño de las 3 imágenes.
4. Desde NASM, se capturan los parámetros enviados desde C de la siguiente manera:
    - **EAX**: Imagen 1
    - **EBX**: Imagen 2
    - **ECX**: Imagen Máscara
    - **EDX**: Tamaño de las imágenes
5. Se utiliza **EDX** para moverse en los vectores almacenados de las imágenes, en un principio se resta 8 posiciones para empezar trabajar con las últimas 8 posiciones de las imágenes.
6. Se crea un bucle para poder recorrer las posiciones de las imágenes desde el final al inicio, restando de a 8 posiciones.
7. Dentro del bucle se utiliza instrucciones SIMD para el desarrollo de la lógica del enmascaramiento
8. Se mueven 8 bytes de las 3 imágenes en registros **MMX** distintos y se realizan las operaciones:
    - **PAND**: Entre la Imagen 2 y la máscara para obtener un registro **MMX** con las posiciones de pixeles blancos sobreescritos por los pixeles de la Imagen 2.
    - **PANDN**: Entre la Imagen 1 y la máscara para obtener otro registro **MMX** pero con las posiciones de pixeles negros sobreescritos por los pixeles de la Imagen 1.
    - **POR**: Se realiza un OR lógico entre los registros resultados de las operaciones **PAND** y **PANDN** para obtener combinar los resultados y obtener el resultado final del enmascaramiento.
9. Una vez que el bucle finaliza se devuelve el control al programa desarrollado en C y realiza la escritura de la nueva imagen reescrita en el buffer de la Imagen 1.

## Problemas encontrados

Los siguientes obstáculos fueron encontrados en el desarrollo de la lógica desde NASM:

- Pasaje de parámetros de tipo unsgined char desde C a NASM.
- Problemas de violación de segmento en C.
- Emascaramiento inverso al solicitado con las instrucciones SIMD.
- Problemas al reescribir el buffer de la Imagen 1 desde NASM.
- Problemas al utilizar el método clocks() para el cálculo de tiempo de ejecución de los métodos de enmascaramiento de C y NASM.

## Demo

**Ejecución**
Para correr el programa basta con ejecutar el siguiente script:

```bash
cd enmascarar-con-simd
sh bash.sh
```

Se realizaron 4 pruebas para la ejecucion de los enmascarar, con distintas imagenes, de 512x512 y 1980x1080.

**512x512**

![salida_c512x512](/uploads/07811b9e52e0d5c5af2eec5bc6e845ba/salida_c512x512.PNG)

**1980x1080**

![salida_c1920x1080](/uploads/c7b16ecab0a323d32a4c8adeed2b8628/salida_c1920x1080.png)

**Tiempos de ejecución**

Los siguiente gráfico se muestra la comparación de tiempo de ejecución en segundos entre las funciones *enmascarar_c* y *enmascarar_asm* con imágenes de tamaños 512x512 y 1980x1080.

![grafico_512x512](/img/doc/512x512-grafico.png)

![grafico_512x512](/img/doc/1920x1080-grafico.png)

Como muestran los gráficos, utilizar instrucciones SIMD para trabajar con paralelismo a nivel de datos se procesa en menor tiempo 3 imágenes para realizar un enmascaramiento.

## Autores (Grupo 6)

- **Avila Facundo Mauricio** - faku1234faku@gmail.com
- **De Benedetti Lautaro** - lautarodebenedetti@gmail.com