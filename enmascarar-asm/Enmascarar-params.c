#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*prototipos*/
extern int leer_rgb(char *archivo, unsigned char *buffer, int cant);
extern int escribir_rgb(char *archivo, unsigned char *buffer, int cant);
extern void enmascarar_asm(unsigned char *buffer1, unsigned char *buffer2, unsigned char *mascara, int tamanioImg);

void enmascarar_s ( unsigned char *a, unsigned char *b, unsigned char *mask, unsigned char *resultado, int cant ) {
    unsigned char *bufferA = NULL;
	unsigned char *bufferB = NULL;
	unsigned char *bufferMascara = NULL;

	/** Pedimos memoria***/
	bufferA = malloc(cant*3);
	bufferB = malloc(cant*3);
	bufferMascara = malloc(cant*3);

    /*** Leemos los archivos rgb y guardamos el rgb en el buffer ***/
	leer_rgb(a, bufferA, cant*3);
	leer_rgb(b, bufferB, cant*3);
	leer_rgb(mask, bufferMascara, cant*3);

	enmascarar_asm(bufferA, bufferB, bufferMascara, cant*3);

    escribir_rgb(resultado, bufferA, cant*3);
    
	free(bufferA);
	free(bufferB);
	free(bufferMascara);
}