;comandos para compilar
;nasm -f elf32 enmascarar-asm.s -o enmascarar-asm.o
;gcc -m32 -o ../img/ejecutable enmascarar-asm.o Enmascarar-params.c 
;./ejecutable img1.rgb img2.rgb mascara.rgb 512 512

section .text
global enmascarar_asm

enmascarar_asm:
    ;write your code here
    push EBP
    
    mov EBP, ESP
    
    ;imgA      
    mov eax, [ebp+8]
    
    ;imgB
    mov ebx, [ebp+12]
    
    ;imgMascara
    mov ecx, [ebp+16]
    
    ;tamaño img
    mov edx, [ebp+20]
    sub edx, 8
    
    iterar:
    ;Lógica de Enmascarado
        
        ;Muevo 8 bytes de la imgB
        movq mm4, [ebx+edx]
        
        ;Muevo 8 bytes de la mascara
        movq mm1, [ecx+edx]
        
        ;Piso los pixeles blancos con la imgB
        pand mm4, mm1
        
        ;Muevo 8 bytes de la imgA
        movq mm3, [eax+edx]
        
        ;Piso los pixeles negros con la imgA
        pandn mm1, mm3
        
        ;Guardo en mm4 la combinación de las imágenes
        por mm4, mm1
        
        ;Sobreescribo los 8 valores en la dirección de memoria en curso
        movq [eax + edx], mm4
        
        sub edx, 8
        
        cmp edx, 0
        jge iterar
    
    
    mov EBP, ESP
    pop EBP
    
    ret