#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <time.h>

extern void enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, unsigned char *resultado, int cant);
extern void enmascarar_s(unsigned char *a, unsigned char *b, unsigned char *mask, unsigned char *resultado, int cant);
int leer_rgb(char *archivo, unsigned char *buffer, int cant);
int escribir_rgb(char *archivo, unsigned char *buffer, int cant);

/***Abre el archivo, lo lee y guarda en el buffer, luego cierra el archivo***/
int leer_rgb(char *archivo, unsigned char *buffer, int cant) {
	FILE *file = NULL;
	file = fopen(archivo,"rb");				//para leer el archivo
	fread(buffer,1,cant,file);
	fclose(file);
	return 0;
}

/***Abre el archivo, le escribe el resultado del buffer y luego cierra el archivo***/
int escribir_rgb(char *archivo, unsigned char *buffer, int cant) {
	FILE *file = NULL;
	file = fopen(archivo,"wb");  	//para leer el archivo y para escribir wb
	fwrite(buffer,1,cant,file);
	fclose(file);
	return 0;
}

int main(int argc,char *argv[]) {
	/*cargamos los valores ingresados por la linea de comandos en las variables*/
	char *imgA = argv[1]; //img1.rgb
	char *imgB = argv[2]; //img2.rgb
    char *mascara = argv[3];//mascara.rgb
    char *salida_c = argv[4];//salida_c.rgb
    char *salida_asm = argv[5];//salida_asm.rgb
	int filas = strtol(argv[6], NULL, 10); //ancho
	int columnas = strtol(argv[7], NULL, 10); // alto

	clock_t begin = clock();
	enmascarar_c( imgA, imgB, mascara, salida_c, filas * columnas);
	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Tiempo de ejecucion del enmascarar_c: %f\n", time_spent);
	
	clock_t begin2 = clock();
	enmascarar_s( imgA, imgB, mascara, salida_asm, filas * columnas);
	clock_t endDummy = clock();

	printf("\n");
	double time_spentDummy = (double)(endDummy - begin2) / CLOCKS_PER_SEC;

	clock_t end2 = clock();
	double time_spent2 = (double)(end2 - begin2) / CLOCKS_PER_SEC;
	printf("Tiempo de ejecucion del enmascarar_s: %f\n", time_spent2);
}
