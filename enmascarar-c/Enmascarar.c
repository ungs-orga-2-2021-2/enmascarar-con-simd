#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

/*prototipos*/
extern int leer_rgb(char *archivo, unsigned char *buffer, int cant);
extern int escribir_rgb(char *archivo, unsigned char *buffer, int cant);
void aplica_mascara(unsigned char *a, unsigned char *b, unsigned char *mask, unsigned char *resultado, int cant);
void enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, unsigned char *resultado, int cant);

/*** Cuando es blanco 255, tomo el valor del rgb b sino del a ***/
void aplica_mascara(unsigned char *a, unsigned char *b, unsigned char *mascara, unsigned char *resultado, int cant) {
	int i;
	for (i=0; i<cant; i++){
		if(mascara[i] == 255) {
			resultado[i] = b[i];
		} else {
			resultado[i] = a[i];
		}
	}
} 		

/*** Realiza el proceso de enmascarar ***/
void enmascarar_c ( unsigned char *a, unsigned char *b, unsigned char *mask, unsigned char *resultado, int cant ) {
	/***Inicializamos el buffer de cada imagen.rgb ***/
	unsigned char *bufferA = NULL;
	unsigned char *bufferB = NULL;
	unsigned char *bufferMascara = NULL;
	unsigned char *bufferResultado = NULL;

	/** Pedimos memoria***/
	bufferA = malloc(cant*3);
	bufferB = malloc(cant*3);
	bufferMascara = malloc(cant*3);
	bufferResultado = malloc(cant*3);

	/*** Leemos los archivos rgb y guardamos el rgb en el buffer ***/
	leer_rgb(a, bufferA, cant*3);
	leer_rgb(b, bufferB, cant*3);
	leer_rgb(mask, bufferMascara, cant*3);
	
	/*** Aplicamos la mascara y generamos el resultado de la imagen final ***/
	aplica_mascara(bufferA, bufferB, bufferMascara, bufferResultado, cant*3);

	/*** Escribimos el resultado ***/
	escribir_rgb(resultado, bufferResultado, cant*3);
	
	free(bufferA);
	free(bufferB);
	free(bufferMascara);
	free(bufferResultado);
};
